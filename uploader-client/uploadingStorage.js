const fs = require('fs');
const path = require('path');
const uploads = require('./uploads.json');

const uploadings = new Map(uploads.map(uploading => [uploading.uid, uploading]));

module.exports.get = function (uid) {
    return uploadings.get(uid);
};

module.exports.save = function (uploadingInfo) {
    uploadings.set(uploadingInfo.uid, uploadingInfo);
    saveToFile();
};

module.exports.remove = function (uid) {
    uploadings.delete(uid);
    saveToFile();
};

module.exports.uploadings = function () {
    return new Map(uploadings);
};

module.exports.size = function () {
    return uploadings.size;
};

module.exports.clear = function () {
    uploadings.clear();
    saveToFile();
};

function saveToFile() {
    fs.writeFileSync(path.resolve(__dirname + '/uploads.json'),
        JSON.stringify(uploadings.size > 0 ? Array.from(uploadings.values()) : []));
}

