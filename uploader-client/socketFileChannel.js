const net = require('net');
const fs = require('fs');
const events = require('events');

const socketFileChannel = Object.create(new events.EventEmitter);
socketFileChannel.SOCKET_ERR_EVENT = 'socketErr';
socketFileChannel.FILE_ERR_EVENT = 'fileErr';
socketFileChannel.TRANSFER_EVENT = 'transfer';
socketFileChannel.EOF_EVENT = 'eof';
socketFileChannel.UPLOADED_EVENT = 'uploaded';

socketFileChannel.pipe = function (socket, uploadingInfo) {
    socket.on('error', err => socketFileChannel.emit(socketFileChannel.SOCKET_ERR_EVENT, err, uploadingInfo));

    const readStream = fs.createReadStream(uploadingInfo.filePath, {start: uploadingInfo.position ? uploadingInfo.position : 0});
    readStream.on('data', data => {
        if (data) {
            const isFreeSystemBuffer = socket.write(data, () => socketFileChannel.emit(socketFileChannel.TRANSFER_EVENT, uploadingInfo, data));
            if (!isFreeSystemBuffer) {
                readStream.pause();
                socket.once('drain', () => readStream.resume());
            }
        }
    });

    readStream.on('end', () => {
        socket.end();
        socketFileChannel.emit(socketFileChannel.EOF_EVENT, uploadingInfo);
        socket.once('end', () => {
            socketFileChannel.emit(socketFileChannel.UPLOADED_EVENT, uploadingInfo);
            socket.destroy();
        })
    });

    readStream.on('error', err => socketFileChannel.emit(socketFileChannel.FILE_ERR_EVENT, err, uploadingInfo));
};

module.exports = socketFileChannel;
