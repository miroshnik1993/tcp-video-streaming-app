const fs = require('fs');
const readline = require('readline');
const pathUtil = require('path');
const config = require('./config/config.json');

const uploadingStorage = require('./uploadingStorage');
const uploadingManager = require('./uploadingManager');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

if (uploadingStorage.size() > 0) {     //Undelivered files
    const uploadInfoList = Array.from(uploadingStorage.uploadings().values());
    // uploadInfoList.forEach(uploadInfo => uploadingManager.upload(uploadInfo));
    rl.question('U have not completed uploading: '
        + uploadInfoList.toString()
        + '\n Do u want to try continue upload specified files (Y/N)', (answer) => {
        let a = answer.toUpperCase();
        if (a === 'Y') {
            uploadInfoList.forEach(uploadInfo => uploadingManager.upload(uploadInfo));
        } else if (a === 'N') {
            uploadingStorage.clear();
            askForFiles();
        } else {
            console.log('No no no. I want \'Y\' or \'N\'. Bye... ');
            process.exit(1);
        }
    });
} else {
    askForFiles();
}

function askForFiles() {
    rl.question('Enter file paths to upload on server (separate with blank):\n', answer => {
        const paths = answer.split(' ');
        paths.forEach(p => prepareUploadingInfo(p, uploadingManager.upload));
    });
    // const paths = [
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\1.mp4',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\2.mp4',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\3.mp4',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\4.mp4',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\5.mp4',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\6.ts',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\7.mp4',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\8.webm',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\9.mp4',
    //     'C:\\dev\\java\\workspace\\tcp-video-streaming-app\\videos\\test\\10.mp4'];
    // paths.forEach(p => prepareUploadingInfo(p, uploadingManager.upload));
}

function prepareUploadingInfo(path, callback) {
    fs.stat(path, (err, stats) => {
        if (!err) {
            let size = stats.size;
            if (!stats.isFile()) {
                console.log('Path is not a file: ', path);
            } else if (size > 5e+8) {
                console.log('Sorry, but you can not upload so big files in free version of the program. ' +
                    'Please buy premium version. Path: ', path);
            } else {
                console.log('File exists: ' + path);
                const uploadingInfo = {
                    filePath: path,
                    fileSize: size,
                    fileName: pathUtil.basename(path),
                    host: config.host
                };
                callback(uploadingInfo);
            }
        } else if (err.code === 'ENOENT') {
            console.log('File does not exists: ', path);
        } else {
            console.log('Bad path: ', path);
        }
    });
}