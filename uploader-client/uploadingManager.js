const net = require('net');

const config = require('./config/config.json');
const uploadingStorage = require('./uploadingStorage');

const socketFileChannel = require('./socketFileChannel');
socketFileChannel.on(socketFileChannel.SOCKET_ERR_EVENT, onSocketErr);
socketFileChannel.on(socketFileChannel.FILE_ERR_EVENT, onFileErr);
socketFileChannel.on(socketFileChannel.EOF_EVENT, emittedUploadInfo => console.log("File was sent: ", emittedUploadInfo.filePath));
socketFileChannel.on(socketFileChannel.UPLOADED_EVENT, onUpload);
socketFileChannel.once(socketFileChannel.TRANSFER_EVENT, uploadingStorage.save);
// socketFileChannel.on(socketFileChannel.TRANSFER_EVENT,
//     (emittedUploadInfo, data) => console.log("Transferred ", data.length + ' of ' + emittedUploadInfo.filePath));

module.exports.upload = function upload(uploadingInfo) {
    if (!uploadingInfo.uid) {  //new uploading
        newUpload(uploadingInfo);
    } else {    //try to restore connection
        reestablishUpload(uploadingInfo)
    }
};

function newUpload(uploadingInfo) {
    requestTcpPort(uploadingInfo, uploadingInfo => {
        const socket = provideSocket(uploadingInfo.port);
        requestUid(socket, uploadingInfo, socketFileChannel.pipe.bind(null, socket));
    });
}

function reestablishUpload(uploadingInfo) {
    const socket = provideSocket(uploadingInfo.port);
    requestFileSize(socket, uploadingInfo, socketFileChannel.pipe.bind(null, socket));
}

function requestTcpPort(uploadingInfo, callback) {
    const socket = net.createConnection(config.tcpRouterPort);
    socket.once('data', data => {
        const uploaderServerPort = data.readUIntBE(0, 4);
        console.log('received tcp video server port = ', uploaderServerPort + ' for ' + uploadingInfo.filePath);
        uploadingInfo.port = uploaderServerPort;
        socket.end();
        socket.destroy();
        return callback(uploadingInfo);
    });
    socket.on('error', err => console.log('error during tcp port request: ', err));
}

function requestUid(socket, uploadingInfo, callback) {
    let fileName = uploadingInfo.fileName;
    const uploadingInfoData = Buffer.alloc(1 + fileName.length);
    const pos = uploadingInfoData.writeUInt8(fileName.length);
    uploadingInfoData.write(fileName, pos);
    let uid = '';
    socket.write(uploadingInfoData);
    socket.on('data', function onData(data) {
        uid += data.toString('utf8');
        if (uid.length === config.uidLength) {
            console.log('handshaked. new connection');
            socket.removeListener('data', onData);
            uploadingInfo.uid = uid;
            return callback(uploadingInfo);
        }
    });
}

function requestFileSize(socket, uploadingInfo, callback) {
    const uidData = Buffer.from(uploadingInfo.uid);
    socket.write(uidData);
    socket.once('data', data => {
        const uploadedFileSize = data.readUInt32LE(0);
        uploadingInfo.position = uploadedFileSize;
        console.log('handshaked. connetion restored. Received  fileSize = ', uploadedFileSize);
        callback(uploadingInfo);
    });
}

function provideSocket(port) {
    return net.createConnection({host: config.host, port: port});
}

function onSocketErr(err, emittedUploadInfo) {
    console.log('Error during uploading for: ', emittedUploadInfo.filePath, ' \nError: ', err);
    emittedUploadInfo.retryCount = 0;
    console.log('Trying to reconnect ', ++emittedUploadInfo.retryCount);
    if (emittedUploadInfo.retryCount > 5) {
        console.log("Can not reestablish connection. Cancel uploading.");
        uploadingStorage.remove(emittedUploadInfo.uid);
    } else {
        setTimeout(() => {
            reestablishUpload(emittedUploadInfo);
        }, 2000)
    }
}

function onFileErr(err, emittedUploadInfo) {
    uploadingStorage.remove(emittedUploadInfo.uid);
    console.log('Error during file processing: ', emittedUploadInfo.filePath);
    console.log('Error: ', err);
}

function onUpload(emittedUploadInfo) {
    uploadingStorage.remove(emittedUploadInfo.uid);
    console.log("Uploading was completed: ", emittedUploadInfo.filePath)
}