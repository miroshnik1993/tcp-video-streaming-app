package com.miroshnik.tcpvideostreaming.core.event;

import java.util.Collection;

public interface EventHandler<T extends Event> {

    void handle(T event) throws Exception;

    Collection<Class<? extends Event>> getEventTypes();
}
