package com.miroshnik.tcpvideostreaming.core.tcp;

import java.nio.channels.SelectionKey;

public class SocketChannelHandler {

    private SocketReader socketReader;

    private SocketAccepter socketAccepter;

    public SocketChannelHandler(SocketReader socketReader, SocketAccepter socketAccepter) {
        this.socketReader = socketReader;
        this.socketAccepter = socketAccepter;
    }

    public SocketReader getSocketReader() {
        return socketReader;
    }

    public SocketAccepter getSocketAccepter() {
        return socketAccepter;
    }

    public void handle(SelectionKey key) {
        if (!key.isValid()) {
            return;
        }

        if (socketAccepter != null && key.isAcceptable()) {
            socketAccepter.accept(key);
        }

        if (socketReader != null && key.isReadable()) {
            socketReader.read(key);
        }
    }
}
