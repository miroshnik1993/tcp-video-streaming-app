package com.miroshnik.tcpvideostreaming.core.tcp;

import java.nio.channels.SelectionKey;

public interface SocketReader {
    void read(SelectionKey key);
}
