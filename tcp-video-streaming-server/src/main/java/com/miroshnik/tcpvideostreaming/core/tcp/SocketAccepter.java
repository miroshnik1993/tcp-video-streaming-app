package com.miroshnik.tcpvideostreaming.core.tcp;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.logging.Logger;

public class SocketAccepter {

    private Logger log = Logger.getLogger(this.getClass().getName());

    public void accept(SelectionKey key) {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        SocketChannel channel = null;
        try {
            channel = serverChannel.accept();
            channel.configureBlocking(false);
            Socket socket = channel.socket();
            SocketAddress remoteAddr = socket.getRemoteSocketAddress();

            log.info("Accept: " + remoteAddr);

            subscribeEvents(channel, key);
        } catch (IOException e) {
            log.warning("Unknown error: " + e);
            try {
                channel.close();
                key.cancel();
            } catch (IOException ignore) {
            }
        }
    }

    protected void subscribeEvents(SocketChannel channel, SelectionKey key) throws ClosedChannelException {
        channel.register(key.selector(), SelectionKey.OP_READ);
    }
}
