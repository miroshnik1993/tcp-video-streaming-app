package com.miroshnik.tcpvideostreaming.core.event;

public class EventManagerThreadHolder {

    private static ThreadLocal<EventManager> eventManagerThreadLocal = new ThreadLocal<>();

    public static void hold(EventManager eventManager) {
        eventManagerThreadLocal.set(eventManager);
    }

    public static EventManager eventManager() {
        return eventManagerThreadLocal.get();
    }
}
