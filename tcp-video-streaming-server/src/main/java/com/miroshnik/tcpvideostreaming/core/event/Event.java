package com.miroshnik.tcpvideostreaming.core.event;

public interface Event<T> {
    T getAttachment();
}
