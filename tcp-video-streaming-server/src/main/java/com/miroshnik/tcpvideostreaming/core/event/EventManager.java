package com.miroshnik.tcpvideostreaming.core.event;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.Map;

public class EventManager {

    private Multimap<Class<? extends Event>, EventHandler> eventHandlers = HashMultimap.create();

    public void push(Event event) {
        for (Map.Entry<Class<? extends Event>, EventHandler> entry : eventHandlers.entries()) {
            Class<? extends Event> eventType = entry.getKey();
            if (eventType.equals(event.getClass())
                    || event.getClass().isAssignableFrom(eventType)) {
                try {
                    entry.getValue().handle(event);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public void registerHandler(EventHandler eventHandler) {
        Collection<Class<? extends Event>> eventTypes = eventHandler.getEventTypes();
        for (Class<? extends Event> eventType : eventTypes) {
            getEventHandlers().put(eventType, eventHandler);
        }
    }

    protected Multimap<Class<? extends Event>, EventHandler> getEventHandlers() {
        return eventHandlers;
    }
}
