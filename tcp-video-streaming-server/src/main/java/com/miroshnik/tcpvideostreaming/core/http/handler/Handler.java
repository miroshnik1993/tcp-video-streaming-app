package com.miroshnik.tcpvideostreaming.core.http.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

public interface Handler {
    void handle(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception;

    boolean match(FullHttpRequest request);
}
