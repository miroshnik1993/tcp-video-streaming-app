package com.miroshnik.tcpvideostreaming.core.http;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpResponseStatus;

public interface ErrorSender {
    void sendError(ChannelHandlerContext ctx, HttpResponseStatus status);
}
