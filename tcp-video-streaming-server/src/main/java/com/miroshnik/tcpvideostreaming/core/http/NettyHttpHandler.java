package com.miroshnik.tcpvideostreaming.core.http;

import com.miroshnik.tcpvideostreaming.core.http.handler.HTTPHandlerResolver;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;

import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpResponseStatus.*;

public class NettyHttpHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    private HTTPHandlerResolver handlerResolver;
    private ErrorSender errorSender;

    public NettyHttpHandler(HTTPHandlerResolver handlerResolver, ErrorSender errorSender) {
        this.handlerResolver = handlerResolver;
    }

    public NettyHttpHandler(HTTPHandlerResolver handlerResolver) {
        this.handlerResolver = handlerResolver;
        this.errorSender = BaseErrorSender.INSTANCE;
    }


    @Override
    public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
        if (!request.decoderResult().isSuccess()) {
            errorSender.sendError(ctx, BAD_REQUEST);
            return;
        }

        if (request.method() != GET) {
            errorSender.sendError(ctx, METHOD_NOT_ALLOWED);
            return;
        }

        String uri = request.uri();
        if (uri.endsWith("/")) {
            uri = uri.substring(0, uri.length() - 1);
            request.setUri(uri);
        }

        boolean isResolved = handlerResolver.resolve(ctx, request);

        if (!isResolved) {
            errorSender.sendError(ctx, NOT_FOUND);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        if (ctx.channel().isActive()) {
            errorSender.sendError(ctx, INTERNAL_SERVER_ERROR);
        }
    }
}