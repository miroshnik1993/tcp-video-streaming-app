package com.miroshnik.tcpvideostreaming.core.http.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;

import java.util.Collection;

public class HTTPHandlerResolver {

    private Collection<Handler> handlers;

    public HTTPHandlerResolver(Collection<Handler> handlers) {
        this.handlers = handlers;
    }

    public boolean resolve(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
        for (Handler handler : handlers) {
            if (handler.match(request)) {
                handler.handle(ctx, request);
                return true;
            }
        }

        return false;
    }
}
