package com.miroshnik.tcpvideostreaming.core.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Iterator;
import java.util.logging.Logger;

public abstract class BaseNioTCPServer implements Runnable {
    public static final long DEFAULT_LOAD_FACTOR = 1;

    protected Logger log = Logger.getLogger(this.getClass().getName());

    protected SocketChannelHandler socketChannelHandler;

    private int port;

    public BaseNioTCPServer(int port) {
        this.port = port;
    }

    protected abstract void init();

    public int getPort() {
        return port;
    }

    public Long getLoadFactor() {
        return DEFAULT_LOAD_FACTOR;
    }

    protected void startListening() {
        try {
            init();

            Selector selector = Selector.open();
            ServerSocketChannel serverSocketChannel = createSocketChannel();

            log.info("TCP Streaming Server was started: " + serverSocketChannel.getLocalAddress());

            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            while (selector.isOpen()) {
                selector.select();  // wait for events
                Iterator readyKeys = selector.selectedKeys().iterator();
                while (readyKeys.hasNext()) {
                    SelectionKey key = (SelectionKey) readyKeys.next();
                    readyKeys.remove();
                    socketChannelHandler.handle(key);
                }
            }
        } catch (IOException e) {
            log.warning(e.getMessage());
        }
    }

    public SocketChannelHandler getSocketChannelHandler() {
        return socketChannelHandler;
    }

    @Override
    public void run() {
        try {
            startListening();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ServerSocketChannel createSocketChannel() throws IOException {
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.socket().bind(new InetSocketAddress(port));
        return serverChannel;
    }
}
