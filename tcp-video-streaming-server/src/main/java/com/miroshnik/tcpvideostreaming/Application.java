package com.miroshnik.tcpvideostreaming;

import com.miroshnik.tcpvideostreaming.httpstreaming.server.VideoStreamingHTTPServer;
import com.miroshnik.tcpvideostreaming.tcpuploading.tcp.server.TCPRouter;
import com.miroshnik.tcpvideostreaming.tcpuploading.tcp.server.TCPVideoUploaderServer;
import org.apache.commons.lang3.ArrayUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;

public class Application {

    private static Application INSTANCE;

    private static String DEFAULT_PROP_FILE_PATH = "config.properties";

    private Properties properties = new Properties();

    private List<TCPVideoUploaderServer> videoUploaderServers;

    private VideoStreamingHTTPServer videoStreamingHTTPServer;

    public void start(String... args) throws IOException {
        INSTANCE = this;
        loadProps(ArrayUtils.isEmpty(args) ? DEFAULT_PROP_FILE_PATH : args[0]);
        startTCPUploaderServers();
        startTCPRouter();
        startNioHTTPServer();
    }

    private void startNioHTTPServer() {
        videoStreamingHTTPServer = new VideoStreamingHTTPServer(Integer.valueOf(properties.getProperty("http.port")));
        videoStreamingHTTPServer.start();
    }

    private void startTCPRouter() {
        Integer routerPort = Integer.valueOf(properties.getProperty("tcp.router.port"));
        new Thread(new TCPRouter(routerPort)).start();
    }

    public Properties getProperties() {
        return properties;
    }

    public List<TCPVideoUploaderServer> getVideoUploaderServers() {
        return videoUploaderServers;
    }

    private void startTCPUploaderServers() throws IOException {
        int[] tcpPorts = provideTCPPorts();
        videoUploaderServers = new ArrayList<>(tcpPorts.length);
        for (int tcpPort : tcpPorts) {
            TCPVideoUploaderServer tcpVideoUploaderServer = new TCPVideoUploaderServer(tcpPort);
            videoUploaderServers.add(tcpVideoUploaderServer);
            new Thread(tcpVideoUploaderServer).start();
        }
    }

    private void loadProps(String path) {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(path)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException("Invalid passed path of config = " + path, e);
        }
    }

    private int[] provideTCPPorts() {
        String[] portsProp = properties.getProperty("tcp.ports").split(",");
        return Stream.of(portsProp).mapToInt(Integer::parseInt).toArray();
    }

    public static Application getContext() {
        return INSTANCE;
    }

    public static void main(String[] args) throws IOException {
        Application application = new Application();
        application.start(args);
    }
}
