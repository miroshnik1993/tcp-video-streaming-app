package com.miroshnik.tcpvideostreaming.httpstreaming.handler;

import com.google.gson.Gson;
import com.miroshnik.tcpvideostreaming.Application;
import com.miroshnik.tcpvideostreaming.core.http.handler.Handler;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpResponseStatus.OK;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

public class FileListHandler implements Handler {

    private File rootVideoFolder;
    private Gson gson = new Gson();

    public FileListHandler() {
        rootVideoFolder = new File(Application.getContext().getProperties().getProperty("video.root"));
    }

    @Override
    public void handle(ChannelHandlerContext ctx, FullHttpRequest request) {
        FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, OK);
        response.headers().set(CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON);

        List<String> fileUrls = new LinkedList<>();
        File[] uidDirs = rootVideoFolder.listFiles(dir -> !dir.isHidden() && dir.canRead() && dir.isDirectory());
        if (ArrayUtils.isNotEmpty(uidDirs)) {
            for (File uidDir : uidDirs) {
                File[] files = uidDir.listFiles(f -> !f.isHidden() && f.canRead() && f.isFile() && !f.getName().endsWith(".tmp"));
                if (ArrayUtils.isNotEmpty(files)) {
                    List<String> videoFileUrls = Arrays.stream(files)
                            .map(f -> buildFileUrl(request, f))
                            .collect(Collectors.toList());
                    fileUrls.addAll(videoFileUrls);
                }
            }

            String json = gson.toJson(fileUrls);
            ByteBuf buffer = Unpooled.copiedBuffer(json, CharsetUtil.UTF_8);
            response.content().writeBytes(buffer);
            buffer.release();
        } else {
            response.setStatus(HttpResponseStatus.NO_CONTENT);
        }

        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    private String buildFileUrl(FullHttpRequest request, File file) {
        String url = "http://" + request.headers().get("host") + "/";
        String path = file.getPath();
        if (!File.separator.equals("/")) {
            path = path.replace(File.separator, "/");
        }
        return url + path;
    }

    @Override
    public boolean match(FullHttpRequest request) {
        return request.uri().equals("/videos");
    }
}
