package com.miroshnik.tcpvideostreaming.httpstreaming.server;

import com.google.common.collect.ImmutableList;
import com.miroshnik.tcpvideostreaming.core.http.NettyHttpHandler;
import com.miroshnik.tcpvideostreaming.core.http.NettyHttpServerInitializer;
import com.miroshnik.tcpvideostreaming.core.http.handler.HTTPHandlerResolver;
import com.miroshnik.tcpvideostreaming.httpstreaming.handler.FileListHandler;
import com.miroshnik.tcpvideostreaming.httpstreaming.handler.FileSenderHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

public class VideoStreamingNettyHTTPServerInitializer extends NettyHttpServerInitializer {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        super.initChannel(ch);
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new NettyHttpHandler(
                new HTTPHandlerResolver(ImmutableList.of(new FileListHandler(), new FileSenderHandler()))));
    }
}
