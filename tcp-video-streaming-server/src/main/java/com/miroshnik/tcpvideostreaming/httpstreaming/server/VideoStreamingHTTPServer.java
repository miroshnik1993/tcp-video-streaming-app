package com.miroshnik.tcpvideostreaming.httpstreaming.server;

import com.miroshnik.tcpvideostreaming.core.http.NettyHttpServer;

public class VideoStreamingHTTPServer extends NettyHttpServer {

    public VideoStreamingHTTPServer(int port) {
        super(new VideoStreamingNettyHTTPServerInitializer(), port);
    }
}
