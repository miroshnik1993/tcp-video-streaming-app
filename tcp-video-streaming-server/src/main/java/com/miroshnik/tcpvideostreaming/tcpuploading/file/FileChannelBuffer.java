package com.miroshnik.tcpvideostreaming.tcpuploading.file;

import com.miroshnik.tcpvideostreaming.core.event.EventManagerThreadHolder;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.LinkedList;

public class FileChannelBuffer {

    private LinkedList<ByteBuffer> fileChannelBuffer = new LinkedList<>();

    public boolean isEmpty() {
        return fileChannelBuffer.isEmpty();
    }

    public ByteBuffer poll() {
        ByteBuffer data = fileChannelBuffer.poll();
        notifyChanged(data, FileChannelBufferEvent.OperationType.REMOVE);
        return data;
    }

    public boolean offer(ByteBuffer data) {
        boolean result = fileChannelBuffer.offer(data);
        notifyChanged(data, FileChannelBufferEvent.OperationType.ADD);
        return result;
    }

    public void push(ByteBuffer data) {
        fileChannelBuffer.push(data);
        notifyChanged(data, FileChannelBufferEvent.OperationType.ADD);
    }

    public ByteBuffer getChunk() {
        int chunkSize = fileChannelBuffer.stream()
                .mapToInt(Buffer::remaining)
                .sum();
        ByteBuffer chunk = ByteBuffer.allocate(chunkSize);
        fileChannelBuffer.forEach(chunk::put);
        return chunk;
    }

    private void notifyChanged(ByteBuffer data, FileChannelBufferEvent.OperationType operationType) {
        EventManagerThreadHolder.eventManager().push(new FileChannelBufferEvent(fileChannelBuffer, data, operationType));
    }
}
