package com.miroshnik.tcpvideostreaming.tcpuploading.file;

import com.miroshnik.tcpvideostreaming.tcpuploading.tcp.server.TCPVideoUploaderServer;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class FileUtils {

    private static final Logger log = Logger.getLogger(FileUtils.class.getName());

    public static long getSize(String uid) {
        try {
            Path videoFilePath = getVideoFilePath(uid);
            if (videoFilePath != null) {
                return Files.size(videoFilePath);
            }
        } catch (IOException ignore) {
        }

        return 0;
    }

    public static void remove(String uid) {
        Path videoFilePath = getVideoFilePath(uid);
        if (videoFilePath != null) {
            try {
                Files.delete(videoFilePath);
            } catch (IOException e) {
                log.warning("Exception during deleting: " + e.getMessage() + "\n" + ExceptionUtils.getStackTrace(e));
            }
        }
    }

    public static void removeExt(String uid) {
        Path videoFilePath = getVideoFilePath(uid);
        if (videoFilePath != null) {
            String pathData = videoFilePath.toString();
            try {
                Files.move(videoFilePath, videoFilePath.resolveSibling(pathData.substring(0, pathData.lastIndexOf("."))));
            } catch (IOException e) {
                log.warning("Exception during renaming: " + e.getMessage() + "\n" + ExceptionUtils.getStackTrace(e));
            }
        }
    }

    private static Path getVideoFilePath(String uid) {
        Path fileDirPath = getVideoFileDirPath(uid);
        try {
            try (Stream<Path> stream = Files.walk(fileDirPath)) {
                Path path = stream.filter(Files::isRegularFile).findFirst().orElse(null);
                return path;
            }
        } catch (IOException ignore) {
            return null;
        }
    }

    public static Path getVideoFileDirPath(String uid) {
        return Paths.get(TCPVideoUploaderServer.VIDEO_ROOT_DIR + File.separator + uid);
    }
}
