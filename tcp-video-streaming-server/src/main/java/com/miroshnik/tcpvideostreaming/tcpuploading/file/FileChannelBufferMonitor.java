package com.miroshnik.tcpvideostreaming.tcpuploading.file;

import com.miroshnik.tcpvideostreaming.core.event.Event;
import com.miroshnik.tcpvideostreaming.core.event.EventHandler;

import java.util.Collection;
import java.util.Collections;

/**
 * This event handler is monitoring the buffer fullness
 */
public class FileChannelBufferMonitor implements EventHandler<FileChannelBufferEvent> {

    private Long bulkBuffSize = 0L;

    public Long getBulkBuffSize() {
        return bulkBuffSize;
    }

    @Override
    public void handle(FileChannelBufferEvent event) {
        if (FileChannelBufferEvent.OperationType.ADD == event.getOperationType()) {
            bulkBuffSize++;
        } else {
            bulkBuffSize--;
        }
    }

    @Override
    public Collection<Class<? extends Event>> getEventTypes() {
        return Collections.singletonList(FileChannelBufferEvent.class);
    }
}
