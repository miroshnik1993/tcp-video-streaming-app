package com.miroshnik.tcpvideostreaming.tcpuploading.file;

import com.miroshnik.tcpvideostreaming.core.event.Event;

/**
 * This event will be pushed when file is completely written to disk
 */
public class FileCompletedEvent implements Event<FileCompletedEvent.FileCompletedEventBundle> {

    public static class FileCompletedEventBundle {
        public final String dirName;
        public final String fileName;

        public FileCompletedEventBundle(String dirName, String fileName) {
            this.dirName = dirName;
            this.fileName = fileName;
        }
    }

    private FileCompletedEventBundle fileCompletedEventBundle;

    public FileCompletedEvent(String dirName, String fileName) {
        fileCompletedEventBundle = new FileCompletedEventBundle(dirName, fileName);
    }

    @Override
    public FileCompletedEventBundle getAttachment() {
        return fileCompletedEventBundle;
    }
}
