package com.miroshnik.tcpvideostreaming.tcpuploading.file;

import com.miroshnik.tcpvideostreaming.core.event.EventManagerThreadHolder;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Logger;

/**
 * Writes data to FileChannel.
 * FileChannel management.
 * Buffering of data which could not write to fileChannel at the moment.
 * Flushing information from buffer after uploading is complete.
 */
public class BufferedFileWriter {

    public static final String PROGRESS_FILE_POSTFIX = ".tmp";

    protected Logger log = Logger.getLogger(this.getClass().getName());

    private long nextFilePosition;

    private String dirName;
    private String fileName;

    private FileChannel fileChannel;

    private FileChannelBuffer fileChannelBuffer;

    private AsynchronousFileChannel asynchronousFileChannel;

    private Path targetPath;

    public BufferedFileWriter(String dirName, String fileName, FileChannelBuffer fileChannelBuffer) {
        this.fileChannelBuffer = fileChannelBuffer;
        this.dirName = dirName;
        this.fileName = fileName;
        try {
            targetPath = provideFilePath();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeToFileChannel(ByteBuffer data) throws IOException {
        if (fileChannel == null || !fileChannel.isOpen()) {
            fileChannel = openFileChannel();
        }

        if (fileChannelBuffer.isEmpty()) {
            doWrite(data);
        } else {
            processBufferedData(data);
        }
    }

    public void terminate() {
        try {
            if (fileChannel != null) {
                fileChannel.close();
            }

            if (asynchronousFileChannel != null) {
                asynchronousFileChannel.close();
            }
        } catch (IOException e) {
            log.warning("Error during closing fileChannel");
        }
    }

    public boolean isEmptyBuffer() {
        return fileChannelBuffer.isEmpty();
    }

    private FileChannel openFileChannel() throws IOException {
        return FileChannel.open(targetPath, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
    }

    private void processBufferedData(ByteBuffer data) throws IOException {
        log.info("processBufferedData.");
        fileChannelBuffer.offer(data);
        ByteBuffer bufferedData = fileChannelBuffer.poll();
        while (bufferedData != null && doWrite(bufferedData)) {
            doWrite(bufferedData);
            bufferedData = fileChannelBuffer.poll();
        }
    }

    private boolean doWrite(ByteBuffer data) throws IOException {
        int bytesWritten = fileChannel.write(data);
        nextFilePosition += bytesWritten;
        if (data.hasRemaining()) {
            fileChannelBuffer.push(data);
            return false;
        }

        return true;
    }

    /**
     * Asynchronous cyclic flush until the buffer is empty
     */
    public void flush() throws IOException {
        if (!fileChannelBuffer.isEmpty()) {
            log.info("flushing");
            fileChannel.close();
            asynchronousFileChannel = openAsyncFileChannel();
            ByteBuffer data = fileChannelBuffer.poll();
            asynchronousFileChannel.write(data, nextFilePosition, data, new CompletionHandler<Integer, ByteBuffer>() {
                @Override
                public void completed(Integer result, ByteBuffer attachment) {
                    nextFilePosition += result;
                    if (attachment.hasRemaining()) {
                        fileChannelBuffer.push(attachment);
                    }

                    if (!fileChannelBuffer.isEmpty()) {
                        ByteBuffer nextData = fileChannelBuffer.poll();
                        asynchronousFileChannel.write(nextData, nextFilePosition, nextData, this);
                    } else {
                        completeFlushing();
                    }
                }

                @Override
                public void failed(Throwable exc, ByteBuffer attachment) {
                    log.warning("Error during asynchronousFileChannel writing: " + exc);
                    terminate();
                }
            });
        } else {
            completeFlushing();
        }
    }

    private AsynchronousFileChannel openAsyncFileChannel() throws IOException {
        return AsynchronousFileChannel.open(targetPath, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
    }

    private Path provideFilePath() throws IOException {
        Path dirPath = FileUtils.getVideoFileDirPath(dirName);
        if (!Files.exists(dirPath)) {
            Files.createDirectory(dirPath);
        }

        Path targetPath = Paths.get(dirPath + File.separator + fileName + PROGRESS_FILE_POSTFIX);
        if (!Files.exists(targetPath)) {
            Files.createFile(targetPath);
        }

        return targetPath;
    }

    private void completeFlushing() {
        terminate();
        EventManagerThreadHolder.eventManager().push(new FileCompletedEvent(dirName, fileName));
        FileUtils.removeExt(dirName);
    }
}
