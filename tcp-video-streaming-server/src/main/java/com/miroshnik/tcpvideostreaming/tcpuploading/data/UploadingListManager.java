package com.miroshnik.tcpvideostreaming.tcpuploading.data;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.collect.HashBiMap;

import java.nio.channels.SocketChannel;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Holds and manages data about active/pending/paused uploading.
 * Pending and paused uploading will be cleared after timeout.
 * Client has 3 minutes to reconnect to save his uploading process.
 */
public class UploadingListManager {

    public static final int DEFAULT_TIMEOUT = 60 * 1000 * 3; //3min

    protected Logger log = Logger.getLogger(this.getClass().getName());

    private HashBiMap<SocketChannel, DataUploadingProcess> activeUploadings;
    private Cache<SocketChannel, DataUploadingProcess> pendingUploadings;
    private Cache<String, DataUploadingProcess> pausedUploadings;

    public UploadingListManager(long pausedTimeout, long pendingTimeout) {
        activeUploadings = HashBiMap.create();
        pausedUploadings = createTimeoutCache(pausedTimeout);
        pendingUploadings = createTimeoutCache(pendingTimeout);
    }

    public UploadingListManager() {
        this(DEFAULT_TIMEOUT, DEFAULT_TIMEOUT);
    }

    private <T> Cache<T, DataUploadingProcess> createTimeoutCache(long timeoutMs) {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(timeoutMs, TimeUnit.MILLISECONDS)
                .removalListener((RemovalListener<Object, DataUploadingProcess>) notification -> {
                    if (notification.wasEvicted())
                        notification.getValue().failUploading();
                })
                .recordStats()
                .build();
    }

    public HashBiMap<SocketChannel, DataUploadingProcess> getActiveUploadings() {
        return activeUploadings;
    }

    public Map<SocketChannel, DataUploadingProcess> getPendingUploadings() {
        return pendingUploadings.asMap();
    }

    public Map<String, DataUploadingProcess> getPausedUploadings() {
        return pausedUploadings.asMap();
    }

    public Optional<DataUploadingProcess> pause(SocketChannel channel) {
        Optional<DataUploadingProcess> activeUploading = Optional.ofNullable(activeUploadings.get(channel));
        activeUploading.ifPresent(uploading -> {
            log.info("Pause uploading: " + activeUploading);
            activeUploadings.remove(channel);
            pausedUploadings.put(uploading.getUploadingInfo().getUid(), uploading);
        });

        return activeUploading;
    }

    public Optional<DataUploadingProcess> pending(SocketChannel channel, String uid) {
        Optional<DataUploadingProcess> pausedUploading = Optional.ofNullable(pausedUploadings.getIfPresent(uid));
        pausedUploading.ifPresent(uploading -> {
            log.info("Pending uploading: " + pausedUploading);
            pendingUploadings.put(channel, uploading);
            pausedUploadings.invalidate(uid);
        });

        return pausedUploading;
    }

    public Optional<DataUploadingProcess> activate(SocketChannel channel) {
        Optional<DataUploadingProcess> pendingUploading = Optional.ofNullable(pendingUploadings.getIfPresent(channel));
        pendingUploading.ifPresent(uploading -> {
            log.info("Activate uploading: " + pendingUploading);
            activeUploadings.put(channel, pendingUploading.get());
            pendingUploadings.invalidate(channel);
        });

        return pendingUploading;
    }

    public Optional<DataUploadingProcess> complete(SocketChannel channel) {
        Optional<DataUploadingProcess> activeUploading = Optional.ofNullable(activeUploadings.get(channel));
        activeUploading.ifPresent(uploading -> activeUploadings.remove(channel));
        return activeUploading;
    }

    public void registerUploading(SocketChannel channel, DataUploadingProcess uploadingProcess) {
        pendingUploadings.put(channel, uploadingProcess);
    }
}
