package com.miroshnik.tcpvideostreaming.tcpuploading.tcp;

import com.google.common.collect.HashBiMap;
import com.miroshnik.tcpvideostreaming.core.event.Event;
import com.miroshnik.tcpvideostreaming.core.event.EventHandler;
import com.miroshnik.tcpvideostreaming.tcpuploading.data.DataUploadingProcess;
import com.miroshnik.tcpvideostreaming.tcpuploading.data.UploadingInfo;
import com.miroshnik.tcpvideostreaming.tcpuploading.data.UploadingListManager;
import com.miroshnik.tcpvideostreaming.tcpuploading.file.FileCompletedEvent;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.Collection;
import java.util.Collections;

/**
 * Invoked when the uploading file is completely written to disk
 */
public class CompletedUploadEventHandler implements EventHandler<FileCompletedEvent> {

    private UploadingListManager uploadingListManager;

    public CompletedUploadEventHandler(UploadingListManager uploadingListManager) {
        this.uploadingListManager = uploadingListManager;
    }

    @Override
    public void handle(FileCompletedEvent event) throws IOException {
        SocketChannel socketChannel = provideSocketChannel(event.getAttachment());
        if (socketChannel != null) {
            uploadingListManager.complete(socketChannel);
            socketChannel.shutdownOutput();
            socketChannel.close();
        }

    }

    @Override
    public Collection<Class<? extends Event>> getEventTypes() {
        return Collections.singletonList(FileCompletedEvent.class);
    }

    private SocketChannel provideSocketChannel(FileCompletedEvent.FileCompletedEventBundle bundle) {
        UploadingInfo tmpUploadingInfo = new UploadingInfo(bundle.dirName, bundle.fileName);
        HashBiMap<SocketChannel, DataUploadingProcess> activeUploadings = uploadingListManager.getActiveUploadings();
        return activeUploadings.inverse().get(new DataUploadingProcess(tmpUploadingInfo));
    }
}
