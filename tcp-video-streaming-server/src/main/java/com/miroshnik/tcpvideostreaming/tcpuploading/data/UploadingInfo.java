package com.miroshnik.tcpvideostreaming.tcpuploading.data;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.nio.ByteBuffer;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

public class UploadingInfo {

    public static int MAX_FILE_NAME_SIZE = 255;
    public static int FILE_NAME_SIZE = 1;

    public static int UPLOADING_INFO_SIZE = FILE_NAME_SIZE + MAX_FILE_NAME_SIZE;

    private String uid;
    private String fileName;

    private Pattern fileNamePatter = Pattern.compile("^[^*&%\\s]+$");

    public UploadingInfo(ByteBuffer data) {
        byte fileNameSize = data.get();
        if (data.limit() == FILE_NAME_SIZE + fileNameSize) {
            readFilename(data, fileNameSize);
        }
        data.rewind();
    }

    public UploadingInfo(String uid, String fileName) {
        this.uid = uid;
        this.fileName = fileName;
    }

    public String getUid() {
        return uid;
    }

    public String getFileName() {
        return fileName;
    }

    private void readFilename(ByteBuffer data, byte fileNameSize) {
        byte[] fileNameData = new byte[fileNameSize];
        data.get(fileNameData);
        fileName = new String(fileNameData);
    }

    public String generateUID() {
        return uid = UUID.randomUUID().toString();
    }

    public boolean isValid() {
        return isFileNameValid();
    }

    public boolean isFileNameValid() {
        return StringUtils.isNotEmpty(fileName) && fileNamePatter.matcher(fileName).matches();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UploadingInfo that = (UploadingInfo) o;
        return Objects.equals(uid, that.uid) &&
                Objects.equals(fileName, that.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, fileName);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("uid", uid)
                .append("fileName", fileName)
                .toString();
    }
}
