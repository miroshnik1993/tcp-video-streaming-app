package com.miroshnik.tcpvideostreaming.tcpuploading.tcp.server;

import com.miroshnik.tcpvideostreaming.Application;
import com.miroshnik.tcpvideostreaming.core.tcp.BaseNioTCPServer;
import com.miroshnik.tcpvideostreaming.core.tcp.SocketAccepter;
import com.miroshnik.tcpvideostreaming.core.tcp.SocketChannelHandler;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Finds the least loaded tcp server and send it's port by client's socket
 */
public class TCPRouter extends BaseNioTCPServer {

    public TCPRouter(int port) {
        super(port);
    }

    @Override
    protected void init() {
        socketChannelHandler = new RouterSocketChannelHandler();
    }

    private static class RouterSocketChannelHandler extends SocketChannelHandler {
        private ByteBuffer buffer = ByteBuffer.allocate(8);

        public RouterSocketChannelHandler() {
            super(null, new RouterSocketAccepter());
        }

        @Override
        public void handle(SelectionKey key) {
            super.handle(key);
            if (key.isWritable()) {
                TCPVideoUploaderServer minLoadedServer = findMinLoadedVideoUploaderServer();
                SocketChannel channel = (SocketChannel) key.channel();
                try {
                    buffer.putInt(minLoadedServer.getPort());
                    buffer.flip();
                    channel.write(buffer);
                    buffer.clear();
                } catch (IOException ignore) {
                }
                key.cancel();
            }

        }

        private TCPVideoUploaderServer findMinLoadedVideoUploaderServer() {
            List<TCPVideoUploaderServer> videoUploaderServers = Application.getContext().getVideoUploaderServers();

            Optional<TCPVideoUploaderServer> minLoaded = videoUploaderServers
                    .stream()
                    .min(Comparator.comparing(TCPVideoUploaderServer::getLoadFactor));
            return minLoaded.orElse(videoUploaderServers.iterator().next());
        }
    }

    private static class RouterSocketAccepter extends SocketAccepter {
        @Override
        protected void subscribeEvents(SocketChannel channel, SelectionKey key) throws ClosedChannelException {
            channel.register(key.selector(), SelectionKey.OP_WRITE);
        }
    }

}
