package com.miroshnik.tcpvideostreaming.tcpuploading.data;

import com.miroshnik.tcpvideostreaming.tcpuploading.file.BufferedFileWriter;
import com.miroshnik.tcpvideostreaming.tcpuploading.file.FileChannelBuffer;
import com.miroshnik.tcpvideostreaming.tcpuploading.file.FileUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * Layer between socketChannel and fileChannel.
 * Represents the uploading process.
 * Contains uploading identifier and corresponding file writer.
 */
public class DataUploadingProcess {

    private UploadingInfo uploadingInfo;
    private BufferedFileWriter bufferedFileWriter;

    public DataUploadingProcess(UploadingInfo uploadingInfo) {
        this.uploadingInfo = uploadingInfo;
        bufferedFileWriter = new BufferedFileWriter(uploadingInfo.getUid(), uploadingInfo.getFileName(), new FileChannelBuffer());
    }

    public UploadingInfo getUploadingInfo() {
        return uploadingInfo;
    }

    public BufferedFileWriter getBufferedFileWriter() {
        return bufferedFileWriter;
    }

    public void processData(ByteBuffer data) throws IOException {
        bufferedFileWriter.writeToFileChannel(data);
    }

    public void complete() {
        try {
            bufferedFileWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void failUploading() {
        bufferedFileWriter.terminate();
        FileUtils.remove(uploadingInfo.getUid());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataUploadingProcess that = (DataUploadingProcess) o;
        return Objects.equals(uploadingInfo, that.uploadingInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uploadingInfo);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("uploadingInfo", uploadingInfo)
                .toString();
    }
}
