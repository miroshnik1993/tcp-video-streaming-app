package com.miroshnik.tcpvideostreaming.tcpuploading.file;

import com.miroshnik.tcpvideostreaming.core.event.Event;

import java.nio.ByteBuffer;
import java.util.LinkedList;

/**
 * This event occurs every time the buffer is changed
 */
public class FileChannelBufferEvent implements Event {

    public enum OperationType {ADD, REMOVE}

    private LinkedList<ByteBuffer> fileChannelBuffer;

    private ByteBuffer currData;

    private OperationType operationType;

    public FileChannelBufferEvent(LinkedList<ByteBuffer> fileChannelBuffer, ByteBuffer currData, OperationType operationType) {
        this.fileChannelBuffer = fileChannelBuffer;
        this.currData = currData;
        this.operationType = operationType;
    }

    @Override
    public Object getAttachment() {
        return null;
    }

    public LinkedList<ByteBuffer> getFileChannelBuffer() {
        return fileChannelBuffer;
    }

    public ByteBuffer getCurrData() {
        return currData;
    }

    public OperationType getOperationType() {
        return operationType;
    }
}
