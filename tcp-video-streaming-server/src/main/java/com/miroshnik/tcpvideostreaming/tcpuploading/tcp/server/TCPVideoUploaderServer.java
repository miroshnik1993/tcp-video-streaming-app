package com.miroshnik.tcpvideostreaming.tcpuploading.tcp.server;

import com.miroshnik.tcpvideostreaming.core.event.EventManager;
import com.miroshnik.tcpvideostreaming.core.event.EventManagerThreadHolder;
import com.miroshnik.tcpvideostreaming.core.tcp.BaseNioTCPServer;
import com.miroshnik.tcpvideostreaming.core.tcp.SocketAccepter;
import com.miroshnik.tcpvideostreaming.core.tcp.SocketChannelHandler;
import com.miroshnik.tcpvideostreaming.tcpuploading.data.UploadingListManager;
import com.miroshnik.tcpvideostreaming.tcpuploading.file.FileChannelBufferMonitor;
import com.miroshnik.tcpvideostreaming.tcpuploading.tcp.SocketUploadingReader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TCPVideoUploaderServer extends BaseNioTCPServer {

    public static final String VIDEO_ROOT_DIR = System.getProperty("user.dir") + File.separator + "videos";

    private FileChannelBufferMonitor fileChannelBufferMonitor;

    public TCPVideoUploaderServer(int port) {
        super(port);
    }

    @Override
    protected void init() {
        EventManagerThreadHolder.hold(new EventManager());
        fileChannelBufferMonitor = new FileChannelBufferMonitor();
        EventManagerThreadHolder.eventManager().registerHandler(fileChannelBufferMonitor);
        socketChannelHandler = new SocketChannelHandler(new SocketUploadingReader(), new SocketAccepter());
        createRootVideoDir();
    }

    public Long getLoadFactor() {
        Long bulkBuffSize = fileChannelBufferMonitor.getBulkBuffSize();
        SocketUploadingReader socketReader = (SocketUploadingReader) getSocketChannelHandler().getSocketReader();
        UploadingListManager uploadingListManager = socketReader.getUploadingListManager();
        int activeUploadingsSize = uploadingListManager.getActiveUploadings().size();
        int pausedUploadingsSize = uploadingListManager.getPausedUploadings().size();
        int pendingUploadingsSize = uploadingListManager.getPendingUploadings().size();
        return (activeUploadingsSize + pausedUploadingsSize + pendingUploadingsSize) * bulkBuffSize;
    }

    private void createRootVideoDir() {
        Path rootVideoPath = Paths.get(VIDEO_ROOT_DIR);
        if (!Files.exists(rootVideoPath)) {
            try {
                Files.createDirectory(rootVideoPath);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
