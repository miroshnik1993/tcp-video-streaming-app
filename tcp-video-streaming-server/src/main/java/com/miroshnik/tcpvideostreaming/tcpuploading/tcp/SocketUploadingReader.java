package com.miroshnik.tcpvideostreaming.tcpuploading.tcp;

import com.miroshnik.tcpvideostreaming.core.event.EventManagerThreadHolder;
import com.miroshnik.tcpvideostreaming.core.tcp.SocketReader;
import com.miroshnik.tcpvideostreaming.tcpuploading.data.DataUploadingProcess;
import com.miroshnik.tcpvideostreaming.tcpuploading.data.UploadingInfo;
import com.miroshnik.tcpvideostreaming.tcpuploading.data.UploadingListManager;
import com.miroshnik.tcpvideostreaming.tcpuploading.file.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.logging.Logger;

/**
 * Reads data from socketChannel and transfer it further.
 * Handling new clients, reestablish connections,
 */
public class SocketUploadingReader implements SocketReader {

    public static final int PAYLOAD_SIZE = 4096;
    public static final int UUID_SIZE = 36;
    public static final int FILE_SIZE = 8;

    protected Logger log = Logger.getLogger(this.getClass().getName());

    private UploadingListManager uploadingListManager;

    public SocketUploadingReader() {
        uploadingListManager = new UploadingListManager();
        EventManagerThreadHolder.eventManager().registerHandler(new CompletedUploadEventHandler(uploadingListManager));
    }

    public UploadingListManager getUploadingListManager() {
        return uploadingListManager;
    }

    @Override
    public void read(SelectionKey key) {
        SocketChannel channel = (SocketChannel) key.channel();
        try {
            DataUploadingProcess activeUploading = uploadingListManager.getActiveUploadings().get(channel);
            if (activeUploading != null) {
                upload(key, channel, activeUploading);
            } else {
                DataUploadingProcess pendingUploading = uploadingListManager.getPendingUploadings().get(channel);
                if (pendingUploading != null) {
                    upload(key, channel, uploadingListManager.activate(channel).get());
                } else if (!processNewClientChannel(channel)) {
                    log.warning("Error during reading uploading information: ");
                    disconnectClient(channel, key);
                }
            }
        } catch (Exception e) {
            log.warning("Channel interrupted. Message:" + e.getMessage() + "\nStacktrace: " + ExceptionUtils.getStackTrace(e));
            uploadingListManager.pause(channel);
            disconnectClient(channel, key);
        }
    }

    private void upload(SelectionKey key, SocketChannel channel, DataUploadingProcess uploadingProcess) throws IOException {
        ByteBuffer buff = ByteBuffer.allocate(PAYLOAD_SIZE);
        int readCount = channel.read(buff);
        if (readCount == -1) {  //FIN
            processFIN(key, channel, uploadingProcess);
            return;
        }

        buff.flip();
        uploadingProcess.processData(buff);
    }

    private boolean processNewClientChannel(SocketChannel newChannel) throws IOException {
        ByteBuffer uploadingInfoData = ByteBuffer.allocate(UploadingInfo.UPLOADING_INFO_SIZE);
        newChannel.read(uploadingInfoData);
        uploadingInfoData.flip();

        String uid = retrieveUID(uploadingInfoData);
        if (StringUtils.isNotEmpty(uid)) {
            return tryToReestablishConnection(newChannel, uid);
        } else {
            return registerNewClient(newChannel, uploadingInfoData);
        }
    }

    private void sendUploadedFileSize(SocketChannel channel, long fileSize) throws IOException {
        ByteBuffer fileSizeData = ByteBuffer.allocate(FILE_SIZE);
        fileSizeData.putLong(fileSize);
        fileSizeData.flip();
        channel.write(fileSizeData);
    }

    private boolean tryToReestablishConnection(SocketChannel channel, String uid) throws IOException {
        DataUploadingProcess pausedUploading = uploadingListManager.getPausedUploadings().get(uid);
        if (pausedUploading != null
                && pausedUploading.getBufferedFileWriter().isEmptyBuffer()) {
            long fileSize = FileUtils.getSize(uid);
            sendUploadedFileSize(channel, fileSize);
            uploadingListManager.pending(channel, uid);
            log.info("Reestablish Connection: " + uid);
            return true;
        }
        return false;
    }

    private String retrieveUID(ByteBuffer uploadingInfoData) throws IOException {
        if (uploadingInfoData.limit() != UUID_SIZE) {
            return null;
        } else {
            byte[] uidData = new byte[UUID_SIZE];
            uploadingInfoData.get(uidData);
            return new String(uidData);
        }
    }

    private boolean registerNewClient(SocketChannel channel, ByteBuffer uploadingInfoData) throws IOException {
        UploadingInfo uploadingInfo = new UploadingInfo(uploadingInfoData);
        String uid = uploadingInfo.generateUID();
        if (uploadingInfo.isValid()) {
            channel.write(ByteBuffer.wrap(uid.getBytes()));
            uploadingListManager.registerUploading(channel, new DataUploadingProcess(uploadingInfo));
            log.info("New client: " + uid);
            return true;
        }
        return false;
    }

    private void disconnectClient(SocketChannel channel, SelectionKey key) {
        key.cancel();
        try {
            channel.close();
        } catch (IOException ignore) {
        }
    }

    private void processFIN(SelectionKey key, SocketChannel channel, DataUploadingProcess uploadingProcess) {
        Socket socket = channel.socket();
        SocketAddress remoteAddr = socket.getRemoteSocketAddress();
        log.info("FIN received, remote addr: " + remoteAddr + ", uid: " + uploadingProcess.getUploadingInfo().getUid());
        key.cancel();
        DataUploadingProcess dataUploadingProcess = uploadingListManager.getActiveUploadings().get(channel);
        if (dataUploadingProcess != null) {
            dataUploadingProcess.complete();
        }
    }
}